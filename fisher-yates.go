// Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí.
// Licensed under the MIT License.

package shuffle

import "math/rand"

// FY interface implements the Fisher-Yates shuffle.
type FY interface {
	Len() int
	Swap(int, int)
}

// FisherYates function performs an in-place Fisher-Yates shuffle.
func FisherYates(a FY) {
	var j int
	// rand.Seed(1)
	for i := a.Len() - 1; i > 0; i-- {
		j = rand.Intn(i)
		a.Swap(i, j)
	}
}
